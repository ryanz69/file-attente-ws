var server = require('http').createServer();
var http = require("http");

io = require('socket.io')(server, { origins: '*:*'}),

    port = 8000;

io.on('connection', function (socket) {
    console.log('Socket.io est connecté');


    http.get('http://localhost:8080/api/user/', (resp) => {
        let listeHelp = [];
        resp.on('data', (chunk) => {
            listeHelp += chunk;
        });

        resp.on('end', () => {
            console.log(listeHelp);
            io.emit('help-list', JSON.parse(listeHelp));
        });
    })

    socket.on('disconnect', function () {
        console.log('Socket.io déconnecté');
    });

});

server.listen(port); 